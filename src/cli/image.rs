use clap::{Arg, ArgAction, Command};
use rexiv2::Metadata;
use std::path::Path;

pub fn image() -> Command {
    return Command::new("image")
        .short_flag('i')
        .long_flag("image")
        .about("Lists all EXIF Tags from a photo")
        .arg(
            Arg::new("file")
                .help("file")
                .action(ArgAction::Set)
                .num_args(1),
        )
        .arg(
            Arg::new("search")
                .short('s')
                .long("search")
                .help("Search an specific EXIF tag from a photo")
                .action(ArgAction::Set)
                .num_args(1..),
        );
}

pub fn list_exif_tags(path: &Path) {
    if let Ok(meta) = Metadata::new_from_path(path) {
        if let Ok(exif_tags) = meta.get_exif_tags() {
            for tag in exif_tags {
                print_result(&meta, &tag);
            }
        }
    }
}

pub fn search_exif_tags(path: &Path, tags: &Vec<&str>) {
    if let Ok(meta) = Metadata::new_from_path(path) {
        for tag in tags {
            print_result(&meta, tag);
        }
    }
}

fn print_result(meta: &Metadata, tag: &str) {
    let tag_value = match meta.get_tag_string(tag) {
        Ok(tag_value) => tag_value,
        _ => "".to_string(),
    };
    print!("- {:?}: {:?} \n", tag, tag_value);
}
