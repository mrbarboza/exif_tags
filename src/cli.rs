mod image;

use clap::{ArgMatches, Command};
use std::path::Path;

pub fn create() -> Command {
    return Command::new("exif-tags")
        .version("1.0")
        .about("Listing all exif tags from a photo")
        .author("Miguel Barboza <mrbarboza@icloud.com>")
        .arg_required_else_help(true)
        .subcommand(image::image());
}

pub fn execute(matches: &ArgMatches) {
    match matches.subcommand() {
        Some(("image", img_matches)) => {
            let image = img_matches.get_one::<String>("file").unwrap();
            let path = Path::new(image);

            if img_matches.contains_id("search") {
                let tags = img_matches
                    .get_many::<String>("search")
                    .expect("is present")
                    .map(|s| s.as_str())
                    .collect();
                image::search_exif_tags(path, &tags);
            } else {
                image::list_exif_tags(path);
            }
        }
        Some((&_, _)) => todo!(),
        None => todo!(),
    };
}
