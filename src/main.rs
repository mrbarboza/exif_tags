extern crate rexiv2;

mod cli;

fn main() {
    rexiv2::initialize().expect("Unable to initialize rexiv2");

    let cli = cli::create();
    let matches = cli.get_matches();
    cli::execute(&matches);
}
